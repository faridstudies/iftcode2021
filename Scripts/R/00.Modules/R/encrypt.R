#' Title write.aes
#' @description write encrypted data
#' 
#' @details This is a generic function that can be used to save encrypted information to the file system. 
#' Once data are encrypted by using the key, it is possible to read by using the function read.aes
#' 
#' @param df dataframe with data to be encrypted i.e. data.frame(x = "UserName", y = "Password")
#' @param key key for encryption
#' @param filename file path with file name where the encrypted file is written to
#'
#' @return outputs an encrypted file (.cr)
#' @export
#'
#' @examples \dontrun{
#' Df <- as.data.frame(cbind(userName = "luca.cocco", passWord = "abcd1234"))
#' load("D:\\LucaCocco\\ucl\\iftcode2021\\Scripts\\R\\00.Modules\\sysdata.rda")
#' FileName <- "D:\\LucaCocco\\ucl\\iftcode2021\\Scripts\\R\\00.Modules\\mongodb.cr"
#' write.aes(Df, key, FileName)
#' }

write.aes <-  function(df, key, filename){
  xx <- textConnection("out", "w")
  utils::write.csv(df, xx, row.names = F)
  close(xx)
  
  out <- paste(out, collapse = "\n")
  raw <- charToRaw(out)
  raw <- c(raw, as.raw(rep(0, 16-length(raw)%%16)))
  aes <- digest::AES(key, mode = "ECB")
  
  aes$encrypt(raw)
  writeBin(aes$encrypt(raw), filename)
}


#' Title read.aes
#' @description read.aes function read from encrypted data and returns decrypted data
#' @details in order to perform the decryption, please load the key used for encryption
#' 
#' @param filename file path to location where encrypted data are saved
#' @param key decription/encryption key
#'
#' @return decrypted data
#' @export
#'
#' @examples \dontrun{
#' read.aes(FileName, key)
#' }

read.aes <- function(filename, key){
  dat <- readBin(filename, "raw", n = 1000)
  aes <-  digest::AES(key, mode = "ECB")
  raw <- aes$decrypt(dat, raw = TRUE)
  txt <-  rawToChar(raw[raw > 0])
  read.csv(txt, stringsAsFactors = F)
}



#' Title generate.key
#' @description generate key for encryption/decryption of data
#' 
#' @details this function generates a unique key to encypt/decrypt data. 
#' CAREFUL Notice
#' If this key is lost, changed or deleted, all encrypted/decrypted information generated by using this key won't be retrived anymore.
#' 
#' @param filename 
#'
#' @return
#' @export
#'
#' @examples \dontrun{
#' FileName = "pathtodirectory"
#' generate.key(FileName)
#' }
#' 
generate.key <- function(filename){
  key <- as.raw(sample(1:16, 16))
  save(key, file = filename)
}

#' Title getCredentials
#'
#' @param key encryption/decryption key
#' @param path directory to .cr file
#'
#' @return
#' @export
#'
#' @examples \dontrun{
#' getCredentials(key, path) 
#' }
getCredentials <- function(key, path){
  
  cr.df <- read.aes(path, key)
  
  outputList <- list()
  outputList$userName <- cr.df$x
  outputList$passWord <- cr.df$y
  
  return(outputList)
}