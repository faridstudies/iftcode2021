#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Luca Cocconcelli
# Lecture : 2021-10-16
# Topic   : R Tutorials - 0.Tutorial Introduction to R
# Desc    : in this section, it is provided an overview of functions
#--------------------------------------------------------------------------------------


# Create your first function ----------------------------------------------

MyFirstFunction <- function(x, y){
  if (x > y){
    z = x - y
    return(z)
  } else {
    z = y - x
  }
  z
}

# your first function in practice:
MyFirstFunction(14,17)

# it is possible to setup default values to the argument of a function
MySecondFunction <- function(x = 1, y = 2, Multiply = FALSE){
  if(Multiply){
    # if multiply condition is met, i.e. is TRUE, we will multiply the two variables
    z = x * y
    return(z)
  } else {
    # if condition is set to FALSE, as per defualt, we will sum 
    z = x + y
  }
  z
}

# understand function components ------------------------------------------

# body: the code inside your function
body(MyFirstFunction)

# formals: the list of arguments which controls how you can call the function
formals(MyFirstFunction)

# environment: the map of the location of the function variable
environment(MyFirstFunction)


# Scoping -----------------------------------------------------------------

f <- function(){
  x <- 1
  y <- 2
  x + y
}

f()
x <- 100
f()
rm(f)


f <- function(){
  y <- 2
  x + y
}

rm(f)

f <- function(){
  y <- 2
  i <- function(){
    x + y
  }
  i()
}

f()
rm(f)

f <- function(){
  y <- 2
  function(){
    x + y
  }
}
k <- f()
k()

rm(f, k)
# Functions vs Variables --------------------------------------------------
n <- function(x){ 
  x * x / 2
}

f <- function(){
  n <- 15
  n(n)
}

f()


# return ------------------------------------------------------------------

MySumFunctionOdd <- function(x, y){
  if(!is.numeric(x) | !is.numeric(y)) return("Please, provide numeric arg")

  isOdd <- (x %% 2) + (y %% 2)
  if(!isOdd == 0){
    return(x + y)
    # x + y
  } else {
    print("Not both numbers are odd")
  }
  
  print("End")
}
MySumFunctionOdd(3, 5)

# in R every operation is a function --------------------------------------
x <- 10; y <- 19
x + y

`+`(x, y)

