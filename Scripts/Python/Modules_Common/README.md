# Instructions to encrypt/decrypt passwords

**Please read carefully these instructions.** 
You don't want to share with anyone your username and passwords!

First, import the module:
```
import encrypt

```

Second, create a unique encryption key:

```
generate_key("enckey")

```
This is the unique key that allows you to safely store passwords. Don't share it with anyone.
Once a key is generated, you can use it to encrypt username and password by using a Python dictionary.

```
dict_credentials = {'Username': 'MyUserName', 'Password': 'MyPassword'}
write_encrypted(dict_credentials, _get_key("enckey"), 'my_credentials.cr')
```
**Be extra-careful.** If you generate a new key with the generate_key function and you over-write the current enckey, you won't be able to decrypt the my_credentials.cr. You will need to re-encrypt all information again.

Decrypt and use the encrypted information:

```
read_encrypted("my_credentials.cr", _get_key("enckey"))
```
Now the de-crypted username and password can be used.
